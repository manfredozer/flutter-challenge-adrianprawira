import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'screens/splash.dart';

class FirstBugsApp extends StatelessWidget {
  final bool isRelease;
  const FirstBugsApp(this.isRelease, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextTheme textTheme = Theme.of(context).textTheme;

    return MaterialApp(
      title: isRelease ? "1St Bugs" : "Debug 1St Bugs",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
            seedColor: Colors.deepPurple, brightness: Brightness.light),
        textTheme: GoogleFonts.hindTextTheme(textTheme),
        useMaterial3: true,
      ),
      home: const SplashScreen(),
    );
  }
}
