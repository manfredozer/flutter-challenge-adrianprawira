import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'auth/sc_login.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String templateImageAssetDir = "assets/img";
  String templateLogoAssetDir = "assets/logo";
  bool get isIOS => Platform.isIOS;

  @override
  void initState() {
    super.initState();
    moveTo();
  }

  void moveTo() => Future.delayed(const Duration(seconds: 2)).then(
        (value) => Navigator.of(context).pushReplacement(
          isIOS
              ? CupertinoPageRoute(builder: (_) => const LoginScreen())
              : MaterialPageRoute(builder: (_) => const LoginScreen()),
        ),
      );

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,
            child: Image.asset(
              "$templateImageAssetDir/header-splash.png",
              width: size.width,
            ),
          ),
          Align(
            child: Image.asset("$templateLogoAssetDir/logo.png"),
          ),
          Positioned(
            bottom: 0,
            child: Image.asset(
              "$templateImageAssetDir/footer-splash.png",
              width: size.width,
            ),
          ),
        ],
      ),
    );
  }
}
