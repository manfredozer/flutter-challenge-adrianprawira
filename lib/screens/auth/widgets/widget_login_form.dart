import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _loginFormKey = GlobalKey<FormState>();
  final _userIDController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _visiblePassword = false;

  void _showPopupDialog(
          {required String title,
          required String content,
          bool success = false}) =>
      showDialog(
        context: context,
        builder: (dialogContext) => AlertDialog(
          backgroundColor: success != true
              ? Colors.red.withOpacity(.8)
              : Colors.green.withOpacity(.8),
          title: Text(
            title,
            style: const TextStyle(color: Colors.white),
          ),
          content: Text(
            content,
            style: const TextStyle(fontSize: 15, color: Colors.white),
          ),
          actions: [
            TextButton(
              onPressed: Navigator.of(context).pop,
              style: TextButton.styleFrom(primary: Colors.white),
              child: Text("ok".toUpperCase()),
            ),
          ],
        ),
      );

  void _pressLogin() {
    if (_loginFormKey.currentState!.validate()) {
      return _showPopupDialog(
        title: "Login",
        content: "Selamat! Anda Berhasil Login",
        success: true,
      );
    } else {
      if (_userIDController.text.isEmpty && _passwordController.text.isEmpty) {
        return _showPopupDialog(
            title: "Login Error",
            content: "User ID dan Password Tidak Boleh Kosong!");
      }
      if (_userIDController.text.isEmpty) {
        return _showPopupDialog(
            title: "Login Error", content: "User ID Tidak Boleh Kosong!");
      }
      if (_passwordController.text.isEmpty) {
        return _showPopupDialog(
            title: "Login Error", content: "Password Tidak Boleh Kosong!");
      }
      return _showPopupDialog(
          title: "Login Error", content: "User ID atau Password Salah");
    }
  }

  void _loginFromKeyboard(String val) {
    if (_loginFormKey.currentState!.validate()) {
      return _showPopupDialog(
        title: "Login",
        content: "Selamat! Anda Berhasil Login",
        success: true,
      );
    } else {
      if (_userIDController.text.isEmpty && _passwordController.text.isEmpty) {
        return _showPopupDialog(
            title: "Login Error",
            content: "User ID dan Password Tidak Boleh Kosong!");
      }
      if (_userIDController.text.isEmpty) {
        return _showPopupDialog(
            title: "Login Error", content: "User ID Kosong!");
      }
      if (val.isEmpty) {
        return _showPopupDialog(
            title: "Login Error", content: "Password Kosong!");
      }
      return _showPopupDialog(
          title: "Login Error", content: "User ID atau Password Salah");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Form(
          key: _loginFormKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _userIDController,
                  validator: (val) {
                    if (val == null || val.isEmpty) {
                      return "User ID belum diisi";
                    }
                    return null;
                  },
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(fontStyle: FontStyle.italic),
                    hintText: "User ID",
                    labelText: "User ID",
                    contentPadding: EdgeInsets.zero,
                  ),
                  style: const TextStyle(fontSize: 14),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _passwordController,
                  validator: (val) {
                    if (val == null || val.isEmpty) {
                      return "Password belum diisi";
                    }
                    return null;
                  },
                  keyboardType: TextInputType.text,
                  obscureText: !_visiblePassword,
                  textInputAction: TextInputAction.go,
                  onFieldSubmitted: _loginFromKeyboard,
                  decoration: InputDecoration(
                    hintStyle: const TextStyle(fontStyle: FontStyle.italic),
                    hintText: "Password",
                    labelText: "Password",
                    contentPadding: EdgeInsets.zero,
                    suffixIcon: IconButton(
                      icon: Icon(
                        !_visiblePassword
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: !_visiblePassword
                            ? Colors.grey
                            : Colors.deepPurple.withOpacity(.75),
                        size: 20,
                      ),
                      onPressed: () =>
                          setState(() => _visiblePassword = !_visiblePassword),
                    ),
                  ),
                  style: const TextStyle(fontSize: 14),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 16),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: ElevatedButton(
            onPressed: _pressLogin,
            style: ElevatedButton.styleFrom(
                primary: Colors.deepPurple,
                onPrimary: Colors.white.withOpacity(.5),
                padding: const EdgeInsets.symmetric(horizontal: 50)),
            child: Text(
              'login'.toUpperCase(),
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
