import 'package:flutter/material.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  State<SignUpForm> createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _signUpFormKey = GlobalKey<FormState>();

  final _userIDController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  final _passwordFocus = FocusNode();
  final _confirmPasswordFocus = FocusNode();

  bool _visiblePassword = false;
  bool _visibleConfirmPassword = false;

  void _showPopupDialog(
          {required String title,
          required String content,
          bool success = false}) =>
      showDialog(
        context: context,
        builder: (dialogContext) => AlertDialog(
          backgroundColor: success != true
              ? Colors.red.withOpacity(.8)
              : Colors.green.withOpacity(.8),
          title: Text(
            title,
            style: const TextStyle(color: Colors.white),
          ),
          content: Text(
            content,
            style: const TextStyle(fontSize: 15, color: Colors.white),
          ),
          actions: [
            TextButton(
              onPressed: success != true
                  ? Navigator.of(context).pop
                  : () => Navigator.of(context)
                    ..pop()
                    ..pop(),
              style: TextButton.styleFrom(primary: Colors.white),
              child: Text("ok".toUpperCase()),
            ),
          ],
        ),
      );

  void _pressSignUp() {
    if (_signUpFormKey.currentState!.validate()) {
      return _showPopupDialog(
        title: "Sign Up Success",
        content:
            "Selamat! Anda Berhasil Sign Up Akun.\nSilahkan login menggunakan akun tersebut.",
        success: true,
      );
    } else {
      if (_userIDController.text.isEmpty &&
          _passwordController.text.isEmpty &&
          _confirmPasswordController.text.isEmpty) {
        return _showPopupDialog(
            title: "Sign Up Error",
            content: "User ID dan Password Tidak Boleh Kosong!");
      }
      if (_userIDController.text.isEmpty) {
        return _showPopupDialog(
            title: "Sign Up Error", content: "User ID Tidak Boleh Kosong!");
      }
      if (_passwordController.text.isEmpty) {
        return _showPopupDialog(
            title: "Sign Up Error", content: "Password Tidak Boleh Kosong!");
      }
      if (_passwordController.text !=
          _confirmPasswordController.text) {
        return _showPopupDialog(
            title: "Sign Up Error", content: "Kombinasi Password Tidak Cocok!");
      }
      return _showPopupDialog(
          title: "Sign Up Error", content: "Terjadi Kesalahan!");
    }
  }

  void _signUpFromKeyboard(String val) {
    if (_signUpFormKey.currentState!.validate()) {
      return _showPopupDialog(
        title: "Sign Up",
        content: "Selamat! Anda Berhasil Sign Up Akun",
        success: true,
      );
    } else {
      if (_userIDController.text.isEmpty &&
          _passwordController.text.isEmpty &&
          _confirmPasswordController.text.isEmpty) {
        return _showPopupDialog(
            title: "Sign Up Error",
            content: "User ID dan Password Tidak Boleh Kosong!");
      }
      if (_userIDController.text.isEmpty) {
        return _showPopupDialog(
            title: "Sign Up Error", content: "User ID Tidak Boleh Kosong!");
      }
      if (_passwordController.text.isEmpty) {
        return _showPopupDialog(
            title: "Sign Up Error", content: "Password Tidak Boleh Kosong!");
      }
      if (_passwordController.text !=
          _confirmPasswordController.text) {
        return _showPopupDialog(
            title: "Sign Up Error", content: "Kombinasi Password Tidak Cocok!");
      }
      return _showPopupDialog(
          title: "Sign Up Error", content: "Terjadi Kesalahan!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _signUpFormKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  controller: _userIDController,
                  validator: (val) {
                    if (val == null || val.isEmpty) {
                      return "User ID harus diisi!";
                    }
                    return null;
                  },
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(fontStyle: FontStyle.italic),
                    hintText: "User ID",
                    labelText: "User ID",
                    contentPadding: EdgeInsets.zero,
                  ),
                  style: const TextStyle(fontSize: 14),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: _passwordController,
                  focusNode: _passwordFocus,
                  validator: (val) {
                    if (val == null || val.isEmpty) {
                      return "Password harus diisi!";
                    }
                    return null;
                  },
                  obscureText: !_visiblePassword,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.visiblePassword,
                  onEditingComplete: () => FocusScope.of(context)
                      .requestFocus(_confirmPasswordFocus),
                  decoration: InputDecoration(
                    hintText: "Password",
                    hintStyle: const TextStyle(fontStyle: FontStyle.italic),
                    labelText: "Password",
                    contentPadding: EdgeInsets.zero,
                    suffixIcon: IconButton(
                      onPressed: () =>
                          setState(() => _visiblePassword = !_visiblePassword),
                      icon: Icon(
                        !_visiblePassword
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: !_visiblePassword
                            ? Colors.grey
                            : Colors.deepPurple.withOpacity(.75),
                        size: 20,
                      ),
                    ),
                  ),
                  style: const TextStyle(fontSize: 14),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: _confirmPasswordController,
                  focusNode: _confirmPasswordFocus,
                  validator: (val) {
                    if (val == null || val.isEmpty) {
                      return "Konfirmasi Password Anda!";
                    } else if (val != _passwordController.text) {
                      return "Kombinasi Password Tidak Cocok!";
                    }
                    return null;
                  },
                  obscureText: !_visibleConfirmPassword,
                  textInputAction: TextInputAction.go,
                  keyboardType: TextInputType.visiblePassword,
                  onFieldSubmitted: _signUpFromKeyboard,
                  decoration: InputDecoration(
                    hintStyle: const TextStyle(fontStyle: FontStyle.italic),
                    hintText: "Confirm Password",
                    labelText: "Confirm Password",
                    contentPadding: EdgeInsets.zero,
                    suffixIcon: IconButton(
                      icon: Icon(
                        !_visibleConfirmPassword
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: !_visibleConfirmPassword
                            ? Colors.grey
                            : Colors.deepPurple.withOpacity(.75),
                        size: 20,
                      ),
                      onPressed: () => setState(() =>
                          _visibleConfirmPassword = !_visibleConfirmPassword),
                    ),
                  ),
                  style: const TextStyle(fontSize: 14),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 16),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: ElevatedButton(
            onPressed: _pressSignUp,
            style: ElevatedButton.styleFrom(
                primary: Colors.deepPurple,
                onPrimary: Colors.white.withOpacity(.5),
                padding: const EdgeInsets.symmetric(horizontal: 50)),
            child: Text(
              'sign up'.toUpperCase(),
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
