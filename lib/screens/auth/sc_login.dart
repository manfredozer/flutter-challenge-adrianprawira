import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'sc_signup.dart';
import 'widgets/widget_login_form.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String templateImageAssetDir = "assets/img";
    String templateLogoAssetDir = "assets/logo";

    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            Row(
              children: [
                Image.asset(
                  "$templateImageAssetDir/header-login.png",
                  scale: 1.1,
                ),
                Image.asset(
                  "$templateLogoAssetDir/logo.png",
                  scale: 1.6,
                ),
              ],
            ),
            const SizedBox(height: 80),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Text(
                "Login",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Text("Please sign in to continue."),
            ),
            const SizedBox(height: 16),
            const LoginForm(),
            const SizedBox(height: 80),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: const TextStyle(fontWeight: FontWeight.w500),
                children: [
                  const TextSpan(
                    text: "Don't have an account? ",
                    style: TextStyle(color: Colors.grey),
                  ),
                  TextSpan(
                    text: "Sign Up",
                    style: const TextStyle(
                      color: Colors.redAccent,
                      fontWeight: FontWeight.bold,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const SignUpScreen(),
                          )),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 16),
          ],
        ),
      ),
    );
  }
}
