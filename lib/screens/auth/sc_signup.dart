import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'widgets/widget_signup_form.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String templateImageAssetDir = "assets/img";
    String templateLogoAssetDir = "assets/logo";

    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            Row(
              children: [
                Image.asset(
                  "$templateImageAssetDir/header-login.png",
                  scale: 1.1,
                ),
                Image.asset(
                  "$templateLogoAssetDir/logo.png",
                  scale: 1.6,
                ),
              ],
            ),
            const SizedBox(height: 80),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Text(
                "Sign Up",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Text("Please register data to create your account."),
            ),
            const SizedBox(height: 16),
            const SignUpForm(),
            const SizedBox(height: 80),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: const TextStyle(fontWeight: FontWeight.w500),
                children: [
                  const TextSpan(
                    text: "Already have an account? ",
                    style: TextStyle(color: Colors.grey),
                  ),
                  TextSpan(
                    text: "Login",
                    style: const TextStyle(
                      color: Colors.deepPurple,
                      fontWeight: FontWeight.bold,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = Navigator.of(context).pop,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 24),
          ],
        ),
      ),
    );
  }
}
