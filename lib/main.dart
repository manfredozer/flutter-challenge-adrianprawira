import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'app.dart';

void main() {
  runApp(const FirstBugsApp(kReleaseMode));
}
